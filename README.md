# Blu BOM-RRs Hackathon March 2022

Adrian, Liam, Art, Tyler

The work was done on braches of the mobile app and spring boot backend and run locally:

Front end branch: https://gitlab.com/renorun1/rr-builder-app/-/tree/HACKATHON-AALT

Back end branch: https://gitlab.com/renorun1/spring-boot-backend/-/tree/hackathon_blu_bomrr


## Description

A Bill of Materials (BOM) is a list of materials required for a contractor to complete a project. Our goal was to allow the customer to take the contents of their shopping cart and save it as a BOM. The BOM can then be added to an empty shopping cart in the future. This would allow a customer (on their own) to much more easily order materials, especially when the same set of materials is required for multiple jobs. For example, if a contractor specialized in building decks, they could create a `Deck BOM` for one of their standard deck packages.

The idea of being able to create a BOM is contractor-focused. Contractors will enjoy the ease of using this feature as it can drastically reduce the amount of time spent on the mobile app adding items to their cart one-by-one.

This feature will benefit the company in that it will increase the adoption rate of customers to the mobile app. This self-service feature will lead to higher order throughput.

## Future Improvements

We were only able to create an MVP version of this feature. But some of the future improvements are what would make this feature really functional:

- Swapping related items (easily going from one type of wood to another)
- Predefined BOMs (RenoRun can suggest a deck package BOM)
- Parametric Templates (Being able to increase quantities in proportion for smaller/bigger jobs)
- Sharing BOMs with other contractors
- Project Wizards
- Cloning BOMs to modify
- Add pricing and labour estimates
- Autocreate predefined BOMs based on data
- Ability to tie this in with other contractor focused features
